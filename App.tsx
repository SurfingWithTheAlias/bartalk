/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import MainContainer from './src/MainContainer';

function App(): React.JSX.Element {
  return (
    <MainContainer/>
  );
}

export default App;
